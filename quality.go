package core

// Quality 视频质量
type Quality struct {
	// 最大因子
	Max int `default:"30" json:"max" validate:"required,min=1,max=50"`
	// 最小因子
	Min int `default:"10" json:"min" validate:"required,min=1,max=50"`
	// 使用固定量化因子来量化视频这个是在vbr模式的
	Scale int `default:"25" json:"scale" validate:"required,min=1,max=50"`
}
