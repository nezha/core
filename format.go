package core

import (
	`fmt`
	`path/filepath`

	`github.com/storezhang/gox`
	`github.com/storezhang/transfer`
)

// Format 格式
type Format struct {
	// 类型
	Type Type `json:"type,omitempty" validate:"required,oneof=1 2"`
	// 文件
	File transfer.File `json:"file,omitempty" validate:"required"`
	// 视频配置
	Video Video `json:"video,omitempty" validate:"omitempty,required_if=Type 1"`
	// 音频配置
	Audio Audio `json:"audio,omitempty" validate:"required"`
}

func (f *Format) SegmentFilenamePattern() string {
	return fmt.Sprintf("%s\\.%s|%s-\\d+\\.%s", f.File.Filename, hlsPlaylistExt, f.File.Filename, hlsSegmentExt)
}

func (f *Format) SegmentFilename(home string) string {
	return fmt.Sprintf("%s-%%d.%s", filepath.Join(f.DestDir(home), gox.Filename(f.File.Filename)), hlsSegmentExt)
}

func (f *Format) DestFilename(home string) (filename string) {
	if f.isHls() {
		filename = filepath.Join(f.DestDir(home), fmt.Sprintf(
			"%s.%s",
			gox.Filename(f.File.Filename), hlsPlaylistExt),
		)
	} else {
		if TypeAudio == f.Type {
			filename = filepath.Join(f.DestDir(home), fmt.Sprintf(
				"%s.%s",
				gox.Filename(f.File.Filename), f.Audio.Format),
			)
		} else if TypeVideo == f.Type {
			filename = filepath.Join(f.DestDir(home), fmt.Sprintf(
				"%s.%s",
				gox.Filename(f.File.Filename), f.Video.Format),
			)
		}
	}

	return
}

func (f *Format) DestDir(home string) string {
	return filepath.Join(home, "dest", filepath.Base(f.File.Filename))
}

func (f *Format) isHls() (hls bool) {
	if TypeAudio == f.Type {
		hls = AudioFormatHls == f.Audio.Format
	} else {
		hls = VideoFormatHls == f.Video.Format
	}

	return
}
