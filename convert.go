package core

import (
	`path/filepath`

	`github.com/storezhang/gox`
	`github.com/storezhang/transfer`
)

// Convert 转码
type Convert struct {
	// 编号
	Id int64 `json:"id,string" validate:"required"`
	// 文件
	File transfer.File `json:"file" validate:"required"`
	// 格式列表
	Formats []Format `json:"formats" validate:"required,dive"`
}

func (t *Convert) SrcFilename(home string) string {
	return filepath.Join(t.SrcDir(home), filepath.Base(t.File.Filename))
}

func (t *Convert) SrcDir(home string) string {
	return filepath.Join(home, "src", gox.Filename(t.File.Filename))
}
