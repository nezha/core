package core

const (
	// TypeVideo 视频
	TypeVideo Type = 1
	// TypeAudio 音频
	TypeAudio Type = 2
)

// Type 类型
type Type uint8
