package core

import (
	`github.com/storezhang/transfer`
)

// Watermark 水印
type Watermark struct {
	// 水印文件
	File *transfer.File `json:"file" validate:"required"`
	// 类型
	Type WatermarkType `json:"type" validate:"required"`
}
