package core

// Video 视频配置
type Video struct {
	// 视频码率
	Bitrate string `default:"256k" json:"bitrate,omitempty" validate:"required"`
	// 动态码率配置
	Quality Quality `json:"quality,omitempty" validate:"structonly"`
	// 缓冲区，减少QMax和QMin的波动，取得更好的画质
	// 单位：k
	MaxBufferSize int `default:"128" json:"maxBufferSize,omitempty" validate:"required"`
	// 帧率
	Framerate int `default:"6" json:"framerate,omitempty" validate:"required"`
	// 视频编码器
	Codec string `default:"libx264" json:"codec,omitempty" validate:"required"`
	// 文件封装格式
	Format VideoFormat `default:"hls" json:"format,omitempty" validate:"required,oneof=mp4 hls"`
	// 切片配置
	Hls Hls `json:"hls,omitempty" validate:"structonly,required_with=Format hls"`
	// 视频宽度，如果是0表示不设置（原画输出）
	Width int `json:"width,omitempty"`
	// 视频高度，如果是0表示不设置（原画输出）
	Height int `json:"height,omitempty"`
	// 水印
	Watermark Watermark `json:"watermark,omitempty" validate:"structonly"`
}
