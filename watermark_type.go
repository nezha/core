package core

const (
	// WatermarkTypeImage 图片水印
	WatermarkTypeImage WatermarkType = "image"
	// WatermarkTypeText 文件水印
	WatermarkTypeText WatermarkType = "text"
)

// WatermarkType 水印类型
type WatermarkType string
