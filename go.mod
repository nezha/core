module gitea.com/nezha/core

go 1.16

require (
	github.com/mcuadros/go-defaults v1.2.0
	github.com/storezhang/gox v1.5.5
	github.com/storezhang/transfer v1.1.3
	github.com/storezhang/validatorx v1.0.8
)
