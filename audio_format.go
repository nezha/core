package core

const (
	// AudioFormatMp3 Mp3格式
	AudioFormatMp3 AudioFormat = "mp3"
	// AudioFormatHls Hls分片格式
	AudioFormatHls AudioFormat = "hls"
)

// AudioFormat 视频格式
type AudioFormat string
