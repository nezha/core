package core

const (
	// VideoFormatMp4 Mp4格式
	VideoFormatMp4 VideoFormat = "mp4"
	// VideoFormatHls Hls分片格式
	VideoFormatHls VideoFormat = "hls"
)

// VideoFormat 视频格式
type VideoFormat string
