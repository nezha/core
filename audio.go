package core

// Audio 音频配置
type Audio struct {
	// 音频编码器
	Codec string `default:"libmp3lame" json:"codec,omitempty" validate:"required"`
	// 音频码率
	Bitrate string `default:"16k" json:"bitrate,omitempty" validate:"required"`
	// 动态编码率
	Variable bool `default:"true" json:"variable,omitempty" validate:"required"`
	// 音频采样率
	Rate int `default:"16000" json:"rate,omitempty" validate:"required"`
	// 音频通道
	Channels int `default:"2" json:"channels,omitempty" validate:"required"`
	// 音量
	Volume float64 `default:"3.0" json:"volume,omitempty" validate:"required"`
	// 文件封装格式
	Format AudioFormat `default:"mp3" json:"format,omitempty" validate:"required,oneof=mp3 hls"`
	// 切片配置
	Hls Hls `json:"hls,omitempty" validate:"structonly"`
}
