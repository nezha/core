package core

const (
	// ConvertStatusCreated 已创建
	ConvertStatusCreated ConvertStatus = 1
	// ConvertStatusConverting 转码中
	ConvertStatusConverting ConvertStatus = 2
	// ConvertStatusCompleted 转码完成
	ConvertStatusCompleted ConvertStatus = 3
	// ConvertStatusFailed 转码失败
	ConvertStatusFailed ConvertStatus = 4
)

// ConvertStatus 转码状态
type ConvertStatus int8
