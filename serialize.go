package core

import (
	`encoding/json`

	`github.com/mcuadros/go-defaults`
	`github.com/storezhang/validatorx`
)

// Marshal 序列化
func Marshal(obj interface{}) (bytes []byte, err error) {
	return json.Marshal(obj)
}

// Unmarshal 反序列化，并增加验证和默认值
func Unmarshal(obj interface{}, bytes []byte) (err error) {
	// 反序列化
	if err = json.Unmarshal(bytes, obj); nil != err {
		return
	}
	// 处理默认值
	defaults.SetDefaults(obj)
	// 数据验证
	err = validatorx.Struct(obj)

	return
}
