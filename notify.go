package core

// Notify 通知
type Notify struct {
	// 编号
	Id int64 `json:"id" yaml:"id" xml:"id" validate:"required"`
	// 转码状态
	Status ConvertStatus `json:"status" yaml:"status" xml:"status" validate:"required"`
}
