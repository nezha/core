package core

import (
	`time`
)

type Hls struct {
	// 设置切片的间隔
	Duration time.Duration `default:"45s" json:"time,omitempty" validate:"required"`
	// 参数设置播放列表保存的最多条目，设置为0会保存有切片信息
	// ffmpeg默认为5，所以只获得最后的5个片段
	// 但是这儿不能设置默认值，保持为0，保存所有切片信息
	Size int `json:"size,omitempty"`
}
